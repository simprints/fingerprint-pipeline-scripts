#!/bin/bash
if [ $# -eq 0 ]
	then
		SELF=${0##*/}
		echo "Usage: ${SELF} <mcu name[_subtype]> <hardware_variant> [.extension]"
		echo "e.g. ${SELF} stm e-1 .bin"
		echo "e.g. ${SELF} stm_jtag e-1.hex"
		echo "e.g. ${SELF} cypress e-1.bin"
		echo "e.g. ${SELF} un20 e-1"
		exit
fi

MCU_FULL=$1
MCU_SPLIT=($(echo $MCU_FULL | tr "_" "\n"))
MCU=${MCU_SPLIT[0]}
MCU_SUBTYPE=${MCU_SPLIT[1]}
HARDWARE_VARIANT=$2
EXT=$3
DATE=$(date +'%Y%m%d')
SHORT_COMMIT=$(git rev-parse --short HEAD)
BRANCH=$(git rev-parse --abbrev-ref HEAD)
if [ ${BRANCH} != "master" ] && [ ${BRANCH} != "develop" ]; then
	BRANCH="other_branches/${BRANCH}"
fi
echo ${BRANCH}/${MCU}/${DATE}_${SHORT_COMMIT}/${MCU_FULL}_${HARDWARE_VARIANT}${EXT}
